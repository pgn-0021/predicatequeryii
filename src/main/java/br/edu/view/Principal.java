package br.edu.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.ConfigScope;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;
import com.db4o.query.QueryComparator;

import br.edu.model.Pessoa;

public class Principal {

	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		EmbeddedConfiguration configuration = Db4oEmbedded.newConfiguration();
		configuration.file().generateUUIDs(ConfigScope.GLOBALLY);
				
		//Criando ou conectando com o banco
		ObjectContainer dbCarro = Db4oEmbedded.openFile(configuration, "/home/welder/PessoaCarros/carros.db4o");
		ObjectContainer dbPessoa = Db4oEmbedded.openFile(configuration, "/home/welder/PessoaCarros/pessoas.db4o");
		ObjectContainer dbPessoaCarros = Db4oEmbedded.openFile(configuration, "/home/welder/PessoaCarros/pessoaCarros.db4o");
		
		
		/* 
		 * Adicionando carros as pessoas    
		 
		Pessoa nubia = new Pessoa();
        nubia.setNome("Bruno");
        
        Carro gol = new Carro();
        gol.setPlaca("WBB-0134");
        gol.setNumeroRevisoes(5);
        
        Carro fiat = new Carro();
        fiat.setPlaca("DAF-1100");
        fiat.setNumeroRevisoes(10);
              
        nubia.adicionarCarro(fiat);
        nubia.adicionarCarro(gol);
        
        dbPessoaCarros.store(nubia);
		*/
		
		/*
		 * Buscando uma pessoa
		 */
		/*
		ObjectSet<Pessoa> result = dbPessoaCarros.query(new Predicate<Pessoa>(){

			private static final long serialVersionUID = 1L;

			@Override
			public boolean match(Pessoa pessoa) {
				boolean achou = false;
				for (Carro carro: pessoa.getCarros()){
					if (carro.getPlaca().contains("11")) {
						achou = true;
					} 
				}
				return achou;
			}					
		
		});
		*/

		/**
		 * Retorna a pessoa que tem até dois carros
		 */
		ObjectSet<Pessoa> result = dbPessoaCarros.query(new Predicate<Pessoa>(){

			private static final long serialVersionUID = 1L;

			@Override
			public boolean match(Pessoa pessoa) {
				if (pessoa.getCarros().size() <= 2)
					return true;
				else
					return false;
			}
			
		}, new QueryComparator<Pessoa>(){

			private static final long serialVersionUID = -6470233966535885998L;

			public int compare(Pessoa arg0, Pessoa arg1) {
				return arg0.getNome().compareTo(arg1.getNome());
			}
			
		});
		
		while (result.hasNext()) {
			Pessoa temp = result.next();
			System.out.println("Id: "+dbPessoaCarros.ext().getObjectInfo(temp).getUUID().getLongPart());
			System.out.println("Nome: "+temp.getNome());
			System.out.println("Quant de Carros: "+temp.getCarros().size());
			System.out.println();
		}
				
		dbCarro.close();
		dbPessoa.close();
		dbPessoaCarros.close();
		
		System.out.println("Digite um número para sair do sistema: ");
		Integer i = Integer.parseInt(br.readLine());
		System.out.println(i);
		// make something here!!
		br.close();

	}

}
